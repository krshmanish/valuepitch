const express = require('express');
const app = express();

var http = require('http');
var server = http.createServer(app);

const mongoose = require('mongoose');

const bodyParser = require('body-parser');
const cors = require('cors');
app.use(cors());

// Parse requests to content-type - application/json
app.use(bodyParser.json());
// parse requests to Content-type - application/x-www-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));

// Config database
const configDB = require('./config/config.database');
mongoose.Promise = global.Promise;
// Connect to database
mongoose.connect(configDB.url, { useNewUrlParser: true }).then(() => {
    console.log('Connected to database.');
}).catch(error => {
    console.log('Error connecting to database');
    process.exit();
});

require('./app/routes/courses.routes')(app);

app.get('/', (req, res) => {
    res.json({ message: 'Organize your data' });
});

server.listen(3000, () => {
    console.log('App running.');
});
