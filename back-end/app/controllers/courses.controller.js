const coursesService = require('../services/courses.service');

class CoursesController {

    // Create a course
    create(req, res) {
        if (req.body) {
            coursesService.create(req.body).then(data => {
                res.send(data);
            }).catch(error => {
                res.status(500).send({ message: error.message || 'Unable to create course.' });
            });
        }
    }

    // Get all courses
    findAll(req, res) {
        let data = {
            courses: [],
            totalCount: 0
        }

        coursesService.getCount(req).then(count => {
            data.totalCount = count;
        });

        coursesService.findAll(req).then(courses => {
            data.courses = courses;
            res.send(data)
        }).catch(error => {
            res.status(500).send({ message: error.message || 'Unable to fetch courses...' });
        })
    }

    // Get one course
    findOne(req, res) {
        coursesService.findOne(req).then(course => {
            if (!course) {
                res.status(404).send({ message: 'Course not found with id ' + req.params.courseId });
            }
            res.send(course);
        }).catch(error => {
            if (error.kind === 'objectId') {
                res.status(404).send({ message: 'Course not found with id ' + req.params.courseId });
            }
            res.status(500).send({ message: 'Error fetching course with id ' + req.params.courseId });
        });
    }

    // Update a course
    update(req, res) {
        if (req.body) {
            coursesService.update(req.params.courseId, req.body).then(course => {
                if (!course) {
                    res.status(404).send({ message: 'Course not found with id ' + req.params.courseId });
                }
                res.send(course);
            }).catch(error => {
                if (error.kind === 'objectId') {
                    res.status(404).send({ message: 'Course not found with id ' + req.params.courseId });
                }
                res.status(500).send({ message: 'Error updating course with id ' + req.params.courseId });
            });
        }
    }

    // Delete an course
    delete(req, res) {
        if (req.params.courseId === 'deleteAll') {
            coursesService.deleteAll().then(course => {
                res.send({ message: 'All courses deleted successfully' });
            }).catch(error => {
                if (error.kind === 'objectId' || error.name === 'NotFound') {
                    res.status(404).send({ message: 'Courses not found' });
                }
                res.status(500).send({ message: 'Could not delete all courses' });
            });
        } else {
            coursesService.delete(req).then(course => {
                if (!course) {
                    res.status(404).send({ message: 'Course not found with id ' + req.params.courseId });
                }
                res.send({ message: 'Course deleted successfully' });
            }).catch(error => {
                if (error.kind === 'objectId' || error.name === 'NotFound') {
                    res.status(404).send({ message: 'Course not found with id ' + req.params.courseId });
                }
                res.status(500).send({ message: 'Could not delete course with id ' + req.params.courseId });
            });
        }
    }
}

module.exports = new CoursesController();