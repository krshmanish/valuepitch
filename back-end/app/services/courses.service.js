const Courses = require('../models/courses.model');

class CoursesService {
    create(course) {
        return new Promise((resolve, reject) => {
            Courses.create(course).then(res => {
                resolve(res);
            }).catch(error => {
                reject(error);
            });
        });
    }

    update(courseId, course) {
        return new Promise((resolve, reject) => {
            Courses.findByIdAndUpdate(courseId, {
                title: course.title,
                description: course.description
            }, { new: true }).then(data => {
                resolve(data);
            }).catch(error => {
                reject(error);
            });
        });
    }

    findAll(req) {
        return new Promise((resolve, reject) => {
            const pageNo = req.query.pageNo ? req.query.pageNo : null;
            const sort = req.query.sort ? req.query.sort : null; 
            const order = req.query.order ? req.query.order : null;
            const filterKey = req.query.filterKey ? req.query.filterKey : null;
            const filterValue = req.query.filterValue ? req.query.filterValue : null;
            const obj = {
                [filterKey]: { $regex: filterValue, $options: "i" }
            };
            
            if (pageNo) {
                const resultPerPage = req.query.resultPerPage ? req.query.resultPerPage : 10;                
                Courses.find(filterValue ? obj : {}).sort({[sort]: order}).skip((pageNo - 1) * resultPerPage).limit(parseInt(resultPerPage)).then(courses => {
                    resolve(courses);
                }).catch(error => {
                    reject(error);
                });
            } else {
                Courses.find().sort({[sort]: order}).then(courses => {
                    resolve(courses);
                }).catch(error => {
                    reject(error);
                });
            }
        });
    }

    getCount(req) {
       return new Promise((resolve, reject) =>{
            const filterKey = req.query.filterKey ? req.query.filterKey : null;
            const filterValue = req.query.filterValue ? req.query.filterValue : null;
            const obj = {
                [filterKey]: { $regex: filterValue, $options: "i" }
            };            
            Courses.find(filterValue ? obj : {}).count().then(count => {
                resolve(count)
            }).catch(error => {
                reject(error);
            });
        });
    }

    findOne(req){
        return new Promise((resolve, reject) => {
            Courses.findById(req.params.courseId).then(course => {
                resolve(course);
            }).catch(error => {
                reject(error);
            });
        });
    }

    delete(req) {
        return new Promise((resolve, reject) => {
            Courses.findOneAndDelete({_id: req.params.courseId}).then(course => {
                resolve(course);
            }).catch(error => {
                reject(error);
            });
        });
    }

    deleteAll() {
        return new Promise((resolve, reject) => {
            Courses.deleteMany().then(course => {
                resolve(course);
            }).catch(error =>{
                reject(error);
            });
        });
    }
}

module.exports = new CoursesService();