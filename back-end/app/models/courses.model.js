const mongoose = require('mongoose');
const CoursesSchema = mongoose.Schema({
    title: String,
    description: String,
    authorName: String,
    price: String,
}, {
    timestamps: true
});

module.exports = mongoose.model('Courses', CoursesSchema, 'courses');