module.exports = (app) =>{
    const coursesController = require('../controllers/courses.controller');

    // Add a course
    app.post('/courses', coursesController.create);

    // Get all the courses
    app.get('/courses', coursesController.findAll);

    // Get a course
    app.get('/courses/:courseId', coursesController.findOne);

    // Update a course
    app.put('/courses/:courseId', coursesController.update);

    // Delete a course
    app.delete('/courses/:courseId', coursesController.delete);
}
