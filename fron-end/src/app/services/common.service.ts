import { Injectable } from '@angular/core';
import { HttpService } from './http.service';

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  constructor(private httpService: HttpService) { }

  saveCourseData(data: any) {
    return this.httpService.post('courses', data);
  }

  updateCourseData(id: any, data: any) {
    return this.httpService.put('courses/' + id, data);
  }

  getCourseData(pageNo: number = 1,
    limit: number = 10,
    sort: string = '',
    order: string = 'ASC',
    filterKey: string = '',
    filterValue: string = '') {

    const pageDetails = 'pageNo=' + pageNo + '&&resultPerPage=' + limit;
    const newOrder = order === 'ASC' ? 1 : -1;
    const newSort = 'sort=' + sort + '&&order=' +  newOrder;
    const filter = 'filterKey=' + filterKey + '&&filterValue=' + filterValue;

    return this.httpService.get('courses?' + pageDetails + '&&' + newSort + '&&' + filter);
  }

  getCourseDataById(id: any) {
    return this.httpService.get('courses/' + id);
  }

  deleteCourseDataById(id: any) {
    return this.httpService.delete('courses/' + id);
  }

  deleteAllCourseData() {
    return this.httpService.delete('courses/deleteAll');
  }
}
