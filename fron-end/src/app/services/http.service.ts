
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';

@Injectable()
export class HttpService {
  url = 'http://localhost:3000/';
  userData: any;
  constructor(private http: HttpClient) {
  }

  get(endPoint: string): Observable<any> {
    const url = this.url + endPoint;
    const headers = new HttpHeaders();
    return this.http.get(url, {headers: headers}).pipe(map(res => {
      return res;
    }), catchError((error: any) => {
      return observableThrowError(error.statusText);
    }));
  }

  post(endPoint: string, data: any): Observable<any> {
    const body = JSON.stringify(data);
    const url = this.url + endPoint;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.post(url, body, { headers: headers }).pipe(map(
      res => {
        return res;
      }
    ), catchError((error: any) => {
      return observableThrowError(error.statusText);
    }));
  }

  put(endpoint: string, data: any): Observable<any> {
    const body = JSON.stringify(data);
    const url = this.url + endpoint;
    const headers = new HttpHeaders({
      'Content-Type': 'application/json'
    });
    return this.http.put(url, body, { headers: headers }).pipe(map(res => {
      return res;
    }), catchError(error => {
      return observableThrowError(error.statusText);
    }));
  }

  delete(endPoint: string): Observable<any> {
    const url = this.url + endPoint;
    return this.http.delete(url).pipe(map(res => {
      return res;
    }
    ), catchError((error: any) => {
      return observableThrowError(error.statusText);
    }));
  }
}
