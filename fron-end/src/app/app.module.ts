// Modules
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { NgModule } from '@angular/core';

// Services
import { HttpService, CommonService } from './services';

// Components
import { FormModalComponent } from './form-modal/form-modal.component';
import { PaginationComponent } from './pagination/pagination.component';
import { CardComponent } from './card/card.component';
import { AppComponent } from './app.component';

// Directives and Pipes
import { ClickOutsideDirective } from 'src/directives/click-outside.directive';

@NgModule({
  declarations: [
    AppComponent,
    FormModalComponent,
    PaginationComponent,
    CardComponent,
    ClickOutsideDirective
  ],
  imports: [
    BrowserAnimationsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ToastrModule.forRoot({
      timeOut: 4000,
      positionClass: 'toast-bottom-right',
      preventDuplicates: true,
    }),
  ],
  providers: [
    HttpService,
    CommonService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
