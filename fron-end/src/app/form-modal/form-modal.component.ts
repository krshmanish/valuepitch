import { Component, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-form-modal',
  templateUrl: './form-modal.component.html',
  styleUrls: ['./form-modal.component.less']
})
export class FormModalComponent {

  @Input() formTitle: string;
  @Input() set modalVisibility(state) {
    this.isVisible = state;
  }
  @Input() set formElements(elements) {
    this.controls = elements;
  }
  @Input() set formData(data) {
    this.setFormControls(this.controls, data);
  }
  @Input() set canEditable(isEditable) {
    this.isEditable = isEditable;
  }

  @Output() outsideClick: EventEmitter<boolean> = new EventEmitter();
  @Output() formSubmit: EventEmitter<any> = new EventEmitter();

  controls = [];
  form: FormGroup;
  isVisible: boolean;
  isEditable: boolean;

  constructor(private fb: FormBuilder) { }

  setFormControls(controls, data) {
    const formControls = {};
    for (const control of controls) {
      formControls[control.name] = new FormControl(data ? data[control.name] ? data[control.name] : '' : '', [Validators.required]);
    }
    this.form = new FormGroup(formControls);
  }

  onSubmit() {
    this.formSubmit.emit({
      data: this.form.value,
      isEditable: this.isEditable
    });
  }

  onOverlayClick() {
    this.outsideClick.emit(true);
  }
}
