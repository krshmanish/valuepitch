import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.less']
})
export class CardComponent implements OnInit {
  @Input() keys: any;
  @Input() value: Object;
  @Output() buttonClicked: EventEmitter<any> = new EventEmitter();

  isOptions: boolean;
  constructor() {
    this.isOptions = false;
  }

  ngOnInit() {
  }

  onToggleClick() {
    setTimeout(() => {
      this.isOptions = true;
    }, 100);
  }

  onOutsideClick() {
    this.isOptions = false;
  }

  onClick(option: string, data: any) {
    const emitValue = {
      data: data,
      option: ''
    };
    switch (option) {
      case 'EDIT':
        emitValue.option = 'EDIT';
        break;
      case 'DELETE':
        emitValue.option = 'DELETE';
        break;
      default: break;
    }
    this.buttonClicked.emit(emitValue);
  }
}
