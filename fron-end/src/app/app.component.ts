import { Component, OnInit, OnDestroy } from '@angular/core';
import { CommonService } from './services';
import { Subscription } from 'rxjs';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent implements OnInit, OnDestroy {
  columns = [
    {
      name: 'title',
      label: 'Title',
      isSort: false
    },
    {
      name: 'authorName',
      label: 'Author Name',
      isSort: false
    },
    {
      name: 'description',
      label: 'Description',
      isSort: false
    },
    {
      name: 'price',
      label: 'Price',
      isSort: false
    }
  ];
  isModal: boolean;
  isEditable: boolean;
  selectedData: any;
  formTitle: string;

  courses: any[] = [];
  totalCount: number;
  sortKey: string;
  sortOrder: string;
  filterKey: string;
  filterValue: string;

  subscriptions: Subscription[] = [];
  isPagination: boolean;
  currentPageNo: number = 1;
  pageLimit: number = 10;

  constructor(private commonService: CommonService, private toastService: ToastrService) {
  }

  ngOnInit() {
    this.isPagination = true;
    this.filterKey = this.columns[0].name;
    this.getCourseData();
  }

  onClick(event: any, data: any = {}) {
    this.isModal = false;
    data = typeof (event) === 'object' ? event.data : data;
    switch (typeof (event) === 'object' ? event.option : event) {
      case 'ADD':
        this.isEditable = false;
        this.formTitle = 'Add Course';
        this.isModal = true;
        this.selectedData = data;
        break;
      case 'EDIT':
        this.isEditable = true;
        this.formTitle = 'Edit Course';
        this.selectedData = data;
        this.isModal = true;
        break;
      case 'DELETE':
        this.subscriptions.push(this.commonService.deleteCourseDataById(data._id).subscribe(res => {
          this.toastService.success('Courses deleted successfully.', 'Success');
          this.getCourseData();
        }, error => {
          this.toastService.error('Error while delete course.', 'Error!');
        }));
        break;
      default: break;
    }
  }

  onHeaderClick(selectedHeader: any, index: number) {
    for (let i = 0; i < this.columns.length; i++) {
      if (i !== index) {
        this.columns[i].isSort = false;
      }
    }
    this.columns[index].isSort = !this.columns[index].isSort;
    this.sortKey = selectedHeader.name;
    this.sortOrder = this.columns[index].isSort ? 'DESC' : 'ASC';

    this.getCourseData();
  }

  onFilterClick() {
    this.currentPageNo = 1;
    this.getCourseData();
  }

  getCourseData() {
    this.subscriptions.push(this.commonService.getCourseData(this.currentPageNo,
      this.pageLimit,
      this.sortKey,
      this.sortOrder,
      this.filterKey,
      this.filterValue).subscribe(res => {
      this.getInitialStates();
      this.courses = [];
      this.courses = res.courses;
      this.totalCount = res.totalCount;
    }));
  }

  onPagination(event: any) {
    this.pageLimit = event.pageLimit;
    this.currentPageNo = event.pageNo;
    this.getCourseData();
  }

  onSubmitForm(event: any) {
    console.log(event);

    if (!this.isEditable) {
      this.commonService.saveCourseData(event.data).subscribe(res => {
        this.toastService.success('Course added successfully.', 'Success');
        this.getCourseData();
      }, error => {
        this.toastService.error('Error while save course.', 'Error!');
      });
    } else {
      this.commonService.updateCourseData(this.selectedData._id, event.data).subscribe(res => {
        this.toastService.success('Course updated successfully.', 'Success');
        this.getCourseData();
      }, error => {
        this.toastService.error('Error while update course.', 'Error!');
      });
    }
  }

  onFormOverlayClick(event: any) {
    this.isModal = !event;
  }

  getInitialStates() {
    this.isModal = false;
    this.isEditable = false;
    this.selectedData = {};
  }

  ngOnDestroy() {
    if (this.subscriptions.length) {
      this.subscriptions.forEach(subscription => {
        subscription.unsubscribe();
      });
    }
  }
}
