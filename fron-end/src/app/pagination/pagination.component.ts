import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.less']
})
export class PaginationComponent implements OnInit {

  @Input() set totalCount(totalCount: number) {
    this.total = totalCount;
    this.createPages(false);
  }

  @Input() set pageLimit(limit: number) {
    this.limit = limit;

  }

  @Output() pageDetails: EventEmitter<any> = new EventEmitter();

  isNext: boolean;
  isPrev: boolean;
  pages: number;
  total: number;
  limit: number = 10;
  pageNo: number = 1;
  isInvalidPage: boolean;

  constructor() { }

  ngOnInit() {
    this.isInvalidPage = false;
  }

  createPages(isEmitData: boolean = true) {
    if (this.total <= this.limit) {
      this.isNext = false;
      this.isPrev = false;
    } else {
      this.pages = Math.ceil(this.total / this.limit);
      this.isNext = true;
    }
    if (isEmitData) {
      this.onEmitData();
    }
  }

  onPageCreate() {
    this.createPages();
  }

  onPrevClick() {
    this.pageNo--;
    this.isPrev = true;
    this.isNext = true;
    if (this.pageNo === 1) {
      this.isPrev = false;
    }
    this.onEmitData();
  }

  onNextClick() {
    this.pageNo++;
    this.isNext = true;
    this.isPrev = true;
    if (this.pageNo === this.pages) {
      this.pageNo = this.pages;
      this.isNext = false;
    }
    this.onEmitData();
  }

  onEmitData() {
    const data = {
      pageNo: this.pageNo,
      pageLimit: this.limit
    };
    this.pageDetails.emit(data);
  }

  onKeyDown() {
    if (this.pageNo > this.pages) {
      this.isInvalidPage = true;
    } else {
      this.isInvalidPage = false;
    }
  }
  onLimitKeyDown() {
    this.pageNo = 1;
    this.createPages(false);
  }
}
